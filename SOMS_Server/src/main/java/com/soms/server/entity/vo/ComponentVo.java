package com.soms.server.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value="业务类，封装前端传进来的零件id和数量")
public class ComponentVo {
    @ApiModelProperty(value = "零件id ")
    private Integer id;
    @ApiModelProperty(value = "零件数量 ")
    private int count;
    private String name;

    public ComponentVo(int id, int count) {
        this.id = id;
        this.count = count;
    }
}
