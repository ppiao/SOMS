package com.soms.server.controller;


import com.soms.server.common.JsonResult;
import com.soms.server.entity.TblSupplierComponent;
import com.soms.server.entity.vo.ComponentVo;
import com.soms.server.entity.vo.SupplierComponentVo;
import com.soms.server.mapper.TblSupplierComponentMapper;
import com.soms.server.service.TblOrderService;
import com.soms.server.util.Algorithm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import jdk.nashorn.internal.ir.ReturnNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订购商表 前端控制器
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-11
 */
@CrossOrigin
@RestController
@RequestMapping("/tbl-order")
@Api("订单管理")
public class TblOrderController {

    @Autowired
    TblOrderService tblOrderService;


    @ApiOperation(value = "推荐订单算法")
    @PostMapping(value = "/recommendOrder",produces= MediaType.APPLICATION_JSON_VALUE)
    public JsonResult recommendOrder(@RequestBody
                                         @ApiParam(value = "封装前端传进来的零件id和数量")
                                                 List<ComponentVo> componentVos){
        Map map = tblOrderService.recommendOrder(componentVos);
        return new JsonResult(true,map);
    }
}

