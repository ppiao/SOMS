package com.soms.server.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.soms.server.common.JsonResult;
import com.soms.server.entity.TblComponent;
import com.soms.server.service.TblComponentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 零件表 前端控制器
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-11
 */
@Api(value = "零件种类集操作")
@RestController
@RequestMapping("/tbl-component")
@CrossOrigin
public class TblComponentController {
    @Autowired
    private TblComponentService tblComponentService;


    /*@ApiOperation("零件种类的分页查询")
    @GetMapping("queryAll")
    public JsonResult queryAll(@RequestParam(value = "current") Integer current,@RequestParam("size")Integer size ){
        Page page = new Page();
        page.setCurrent(current).setSize(size);
        Page<TblComponent> page1 = tblComponentService.queryPage(page);
        return new JsonResult(true,page1);
    }*/
    @ApiOperation("零件种类的分页查询")
    @PostMapping("queryAll/{current}/{size}")
    public JsonResult queryAll(@ApiParam(name = "current", value = "当前页", required = true)
                                   @PathVariable Integer current,
                               @ApiParam(name = "size", value = "每页结果数", required = true)
                                   @PathVariable Integer size ){
        Page page = new Page();
        page.setCurrent(current).setSize(size);
        Page<TblComponent> page1 = tblComponentService.queryPage(page);
        return new JsonResult(true,page1);
    }
}

