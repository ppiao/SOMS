package com.soms.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.soms.server.entity.TblComponent;
import com.soms.server.mapper.TblComponentMapper;
import com.soms.server.service.TblComponentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 零件表 服务实现类
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-11
 */
@Service
public class TblComponentServiceImpl extends ServiceImpl<TblComponentMapper, TblComponent> implements TblComponentService {


    @Autowired
    private TblComponentMapper tblComponentMapper;
    @Override
    public List<TblComponent> queryAll() {
        List<TblComponent> tblComponents = tblComponentMapper.selectList(null);
        return tblComponents;
    }

    @Override
    public Page<TblComponent> queryPage(Page page) {
        Page<TblComponent> iPage = tblComponentMapper.selectPage(page, null);
        return iPage;
    }

    @Override
    public List<TblComponent> queryByname(String name) {
        QueryWrapper<TblComponent> wrapper1 = new QueryWrapper<>();
        wrapper1.like("name", name);
        List<TblComponent> tblComponents = tblComponentMapper.selectList(wrapper1);
        return tblComponents;
    }

    @Override
    public int addByname(TblComponent component) throws Exception {
        QueryWrapper<TblComponent> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", component.getName());
        if(tblComponentMapper.selectList(queryWrapper).isEmpty()){
            int insert = tblComponentMapper.insert(component);
            return insert;
        }else{
            throw new Exception("该零件已经存在！");
        }
    }

}
